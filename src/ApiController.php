<?php

namespace Pantagruel74\Yii2Controllers;

use Pantagruel74\Yii2Controllers\traits\ControllerTrait;
use yii\web\Response;

abstract class ApiController extends WebController
{
    use ControllerTrait;

    /**
     * @param $route
     * @param $params
     * @return mixed|void|null
     */
    public function run($route, $params = [])
    {
        try {
            return parent::run($route, $params);
        } catch (\Exception $exception) {
            print_r("{'error': '" . $exception->getMessage() . "'}");die();
        }
    }

    /**
     * @param callable $transaction
     * @return Response
     */
    public function defaultProtocol(callable $transaction): Response
    {
        try {
            $result = $transaction();
            if(!is_array($result) && !is_object($result)) {
                throw new \RuntimeException('Результат выполнения транзакци должен быть массив или Std object');
            }
            return $this->asJson($result);
        } catch (\Exception $exception) {
            return $this->asJson([
                'error' => $exception->getMessage(),
                'details' => $exception->__toString(),
            ]);
        }
    }
}