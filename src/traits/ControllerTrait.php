<?php

namespace Pantagruel74\Yii2Controllers\traits;

use Webmozart\Assert\Assert;
use yii\web\Controller;

trait ControllerTrait
{
    /**
     * @param string $paramName
     * @param string $method
     * @return string
     */
    protected function getParamStrictly(string $paramName, string $method = 'get'): string
    {
        /* @var Controller $this */
        $param = $this->request->$method($paramName);
        Assert::notEmpty($param);
        return $param;
    }

    /**
     * @param string $paramName
     * @param string $method
     * @return string
     */
    protected function getParamSafety(string $paramName, string $method = 'get'): string
    {
        /* @var Controller $this */
        return $this->request->$method($paramName) ?? '';
    }
}