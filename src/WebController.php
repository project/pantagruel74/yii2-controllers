<?php

namespace Pantagruel74\Yii2Controllers;

use Pantagruel74\Yii2Controllers\interfaces\StartPageStrategyInterface;
use Pantagruel74\Yii2Controllers\traits\ControllerTrait;
use yii;

abstract class WebController extends \yii\web\Controller
{
    use ControllerTrait;

    protected StartPageStrategyInterface $startPageStrategy;

    /**
     * @param $id
     * @param $module
     * @param $config
     * @throws yii\base\InvalidConfigException
     */
    public function __construct($id, $module, $config = [])
    {
        $this->startPageStrategy = \Yii::$app->get('startPageStrategy');
        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    abstract public function permissions(): array;

    /**
     * @param $action
     * @return bool
     * @throws yii\web\BadRequestHttpException
     * @throws \Exception
     */
    public function beforeAction($action): bool
    {
        $actionId = $action->id;
        if(isset($this->permissions()[$actionId])) {
            $user = \Yii::$app->user;
            foreach ($this->permissions()[$actionId] as $permission)
            {
                if($user->can($permission)) {
                    return parent::beforeAction($action);
                }
                if(($permission === '*')) {
                    return parent::beforeAction($action);
                }
                if(($permission === '?') && ($user->isGuest === true)) {
                    return parent::beforeAction($action);
                }
                if(($permission === '@') && ($user->isGuest !== true)) {
                    return parent::beforeAction($action);
                }
            }
        }
        $this->goHome();
        return false;
    }

    /**
     * @return yii\web\Response
     */
    public function goHome(): yii\web\Response
    {
        $user = \Yii::$app->user;
        return $this->redirect(yii\helpers\Url::to([$this->startPageStrategy->getStartPageByUser($user)]));
    }

}