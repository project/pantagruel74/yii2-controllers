<?php

namespace Pantagruel74\Yii2Controllers\interfaces;

use yii\web\User;

interface StartPageStrategyInterface
{
    public function getStartPageByUser(User $user): string;
}